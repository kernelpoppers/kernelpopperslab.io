---
id: slack-channel-descriptions
title: Channel Descriptions
external: true
---

## #general

Contains content that all people interested in technology would find interesting (regardless of their location). Posts here should be relatively high quality. Big DSU news posts/info is acceptable.

## #random

Contains anything (that doesn't violate the rules.). No need for quality.

## #dsu

Contains topic relevant specifically to on campus students.

## #help

Post for help about computer topics. Refer important questions to the helpdesk, teachers, or help night.