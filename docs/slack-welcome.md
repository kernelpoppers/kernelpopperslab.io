---
id: slack-welcome
title: Welcome/Rules
external: true
---

The primary component of kernelpoppers is our slack channel. If you have an `@dsu.edu`, `@pluto.dsu.edu`, or `@trojans.dsu.edu` email address, you can freely join. Otherwise, you'll have to contact a community member to join.

Please review the rules before joining participating:

1. Consider all conversations to be public. Many faculty and employers participate in this chat.
2. Be respectful to each other. Do not post any content which could be considered discriminatory/offensive.
3. Your profile should contain the following information:
    - Real name
    - Degree and graduation date
5. Keep conversations in relevant channels (#general, #random, #dsu, #ctf)

Follow this link to join: https://kernelpoppers.slack.com. New members should be referred to this page so that they can review the rules before joining.
