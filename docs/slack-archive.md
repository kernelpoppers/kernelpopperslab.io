---
id: slack-archive
title: Archive
external: true
---

We run an archive bot which allow you to view/search old slack messages. You'll have to be a member of the kernelpoppers group on gitlab to view the page.

URL: https://archive.kernelpoppers.org

Source: https://github.com/gartnera/slack-archiver
