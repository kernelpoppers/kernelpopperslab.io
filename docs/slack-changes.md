---
id: slack-changes
title: Changes
external: true
---

To process any change, create a poll in #changes. There are two types of changes the community can request: rule changes and guideline changes. A rule is something an admin **must** enforce. A guideline is something an admin can choose to enforce.
 - Example of a rule: no pictures of cats are allowed
    - There is no room for interpretation
 - Example of a guideline: content about DSU should be posted in #dsu
    - The admin should use their best judgement about wether to remove the post or direct the user to #dsu

## Common Change Requirements
- Time polls are open: 1 week
- Admin affirmation: at least one admin must vote yes for the change
- Change must be technically possible. Remember we're on the free tier of slack so features are limited.

## Rule Change Requirements
- Respondents required: 50
- Affirmative responses required: 60%

## Guideline Change Requirements
- Respondents required: 25
- Affirmative responses required: 51%