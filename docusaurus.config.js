module.exports = {
  title: 'kernelpoppers',
  tagline: 'A community for Dakota State University (DSU) students, alumni, and faculty',
  url: 'https://kernelpoppers.org',
  baseUrl: '/',
  favicon: 'img/favicon.ico',
  themeConfig: {
    navbar: {
      title: 'kernelpoppers',
      items: [
        {to: 'docs/slack-welcome', label: 'Docs', position: 'left'},
        {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.com/kernelpoppers',
          label: 'Gitlab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [],
      copyright: `Copyright of respective authors. Content on this page does not reflect the official views of DSU. Built with Docusaurus.`,
    },
    colorMode: {
      defaultMode: 'dark',
    }
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
