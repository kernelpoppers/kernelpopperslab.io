module.exports = {
  docs: {
    Slack: ['slack-welcome', 'slack-channel-descriptions', 'slack-changes', 'slack-archive'],
  },
};
